<?php

namespace Controllers;

use Core\View;

class PageController
{
    public function home()
    {
        new View('home', []);
    }
}
