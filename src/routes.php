<?php

use Controllers\AnimalController;
use Controllers\PageController;
use Facades\Route;

Route::get('/', [PageController::class, 'home']);
Route::get('/animals', [AnimalController::class, 'index']);
Route::get('/animals/{id}', [AnimalController::class, 'show']);
Route::post('/animals/create', [AnimalController::class, 'create']);
